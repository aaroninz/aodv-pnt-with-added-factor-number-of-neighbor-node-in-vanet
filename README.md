# AODV PNT with Added Factor Number of Neighbor Node in VANET

Nama: Aaron Setiawan
NRP: 5115100107


Penjelasan Singkat: Merupakan paper modifikasi dari AODV PNT, dengan menambahkan faktor tetangga pada rumus TWR (Total of Weight of the Route)
Pada Hello message di tambahkan faktor TWR untuk menunjukkan kalau rute tersebut merupakan relay node atau rute yang cocok untuk ke destinasi.
Karena modifikasi ini dilakukan pada VANET, ada beberapa faktor yang mempengaruhi TWR:
    1. Speed (s)
    2. Acceleration (a)
    3. Direction (d)
    4. Link Quality Between Node (q)
    dan ada tambahan Jumlah Tetangga Neighbor Nodes (n)
    
Nantinya akan dicari node-node yang eligible dengan beberapa kriteria yang dilihat yakni apakah node tersebut stabil, optimal, atau kedepannya akan lebih baik.
    
Fungsi yang akan diubah antara lain: SendHello, recvHello, recvRequest dengan memasukkan rumus perhitungan TWR

Untuk keperluan laporan akhir kelas Jaringan Nirkabel, pada gitlab terlampir file kodingan AODV-PNT